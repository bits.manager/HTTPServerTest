package se.sameh.server;

import static java.lang.System.getProperty;
import static java.security.KeyStore.getInstance;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.KeyStore;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import se.sameh.engine.SSLEngineManager;

public class Server {
	SSLContext context;
	ServerSocketChannel ssc;
	Selector sel;

	// Server constructor
	public Server() throws Exception {
		// Create the SSLContext
		this.context = SSLContext.getInstance("TLS");

		// Initialize KMF
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		KeyStore ks = getInstance("JKS");
		char[] password = getProperty("javax.net.ssl.keyStorePassword").toCharArray();

		ks.load(new FileInputStream(getProperty("javax.net.ssl.keyStore")), password);
		kmf.init(ks, password);

		context.init(kmf.getKeyManagers(), null, null);

		// Start the server
		this.ssc = ServerSocketChannel.open();
		ssc.configureBlocking(false);
		ssc.socket().bind(new InetSocketAddress((InetAddress) null, 0), 50);
		System.out.println("Server: listening at " + ssc);
		this.sel = Selector.open();
		ssc.register(sel, SelectionKey.OP_ACCEPT);
	}

	public void run() throws IOException {
		// Selector loop
		int count;

		while (sel.keys().size() > 0) {
			try {
				count = sel.select(30 * 1000);
				if (count < 0) {
					System.out.println("Server: select timeout");
					continue;
				}
			} catch (IOException exc) {
				exc.printStackTrace();
				sel.close();
				ssc.close();
				return;
			}

			System.out.println("Server: select count=" + count);
			Set selKeys = sel.selectedKeys();
			Iterator it = selKeys.iterator();

			// process ready keys
			while (it.hasNext()) {
				SelectionKey sk = (SelectionKey) it.next();
				it.remove();
				if (!sk.isValid())
					continue;
				try {
					if (sk.isAcceptable())
						handleAccept(sk);
					if (sk.isReadable())
						handleRead(sk);
					if (sk.isWritable())
						handleWrite(sk);
				} catch (IOException exc) {
					exc.printStackTrace();
					sk.channel().close();
				}
			}
		}
	} // run()

	// Process OP_ACCEPT
	void handleAccept(SelectionKey sk) throws IOException {
		ServerSocketChannel ssc = (ServerSocketChannel) sk.channel();
		SocketChannel sc = ssc.accept();
		if (sc != null) {
			System.out.println("Server: acceptd " + sc);
			sc.configureBlocking(false);

			// Create an SSL engine for this connection
			SSLEngine engine = context.createSSLEngine("localhost", sc.socket().getPort());

			// The server end
			engine.setUseClientMode(false);

			// Create engine manager for the channel & engine
			SSLEngineManager mgr = new SSLEngineManager(sc, engine);

			// Register for OP_READ with mgr as attachment
			sc.register(sel, SelectionKey.OP_READ, mgr);

		}
	}

	// Process OP_READ
	void handleRead(SelectionKey sk) throws IOException {
		SSLEngineManager mgr = (SSLEngineManager) sk.attachment();
		SSLEngine engine = mgr.getEngine();
		ByteBuffer request = mgr.getAppRecvBuffer();
		System.out.println("Server: reading");
		int count = mgr.read(); // remove int
		System.out.println("Server: read count=" + count + " request=" + request);
		if (count < 0) {
			// Client has closed
			mgr.close();
			// Finished with this key
			sk.cancel();
			ssc.close();
		} else if (request.position() > 0) {
			// Client request
			System.out.println("Server: read" + new String(request.array(), 0, request.position()));
			ByteBuffer reply = mgr.getAppSendBuffer();
			request.flip();
			reply.put(request);
			request.compact();
			handleWrite(sk);
		}
	}

	// Process OP_WRITE
	void handleWrite(SelectionKey sk) throws IOException {
		SSLEngineManager mgr = (SSLEngineManager) sk.attachment();
		ByteBuffer reply = mgr.getAppSendBuffer();
		System.out.println("Server: writing " + reply);
		int count = 0;
		while (reply.position() > 0) {
			reply.flip();
			count = mgr.write();
			reply.compact();
			if (count == 0)
				break;
		}
		if (reply.position() > 0) {
			// short write:
			// Register for OP_WRITE and come back here when ready
			sk.interestOps(sk.interestOps() | SelectionKey.OP_WRITE);
		} else {
			// Write succeeded, don't need OP_WRITE any more
			sk.interestOps(sk.interestOps() & ~SelectionKey.OP_WRITE);
		}
	}

	// Main program
	public static void main(String[] args) throws Exception {
		System.setProperty("javax.net.ssl.keyStore", "sam.store");
		System.setProperty("javax.net.ssl.keyStorePassword", "password");

		new Server().run();
		System.out.println("Exiting!");
	}
}

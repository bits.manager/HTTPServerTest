package se.sameh.client;

import java.io.IOException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

public class Client extends Thread {
	SSLSocket socket;
	static SSLContext context;

	public Client(SSLContext context, String host, int port) throws IOException {
		this.socket = (SSLSocket) context.getSocketFactory().createSocket(host, port);
	}

	public void run() {
		try {
			int count;
			byte[] buffer = new byte[8192];

			// Send's request
			socket.getOutputStream().write("hello".getBytes());

			// handshake before read
			socket.startHandshake();

			// Read's reply
			count = socket.getInputStream().read(buffer);
			System.out.println("client: (1) got " + new String(buffer, 0, count) + ":" + count);

			// Get's a new session & do a full handshake
			socket.getSession().invalidate();
			socket.startHandshake();

			// Send's another request
			socket.getOutputStream().write("hello again after new handshake".getBytes());

			// Doing a partial handshake before reading the reply
			socket.startHandshake();

			// Read's the reply
			count = socket.getInputStream().read(buffer);
			System.out.println("client: (2) got " + new String(buffer, 0, count) + ":" + count);
		} catch (IOException exc) {
			exc.printStackTrace();
		} finally {
			try {
				socket.close();
				System.out.println("client: socket closed");
			} catch (IOException exc) {
				// ignored
			}
		}
	}
}

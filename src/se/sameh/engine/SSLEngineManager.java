package se.sameh.engine;

import java.io.IOException;
import java.net.SocketException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

/**
 * SSLEngineManager - manager for SSLEngine
 * 
 * @author usalb
 *
 */
public class SSLEngineManager {
	private SocketChannel channel;
	private SSLEngine engine;
	private ByteBuffer appSendBuffer;
	private ByteBuffer netSendBuffer;
	private ByteBuffer appRecvBuffer;
	private ByteBuffer netRecvBuffer;
	// The current engine result
	private SSLEngineResult engineResult = null;

	/**
	 * SSLEngineManager constructor
	 * 
	 * @param channel
	 *            The underlying channel.
	 * @param engine
	 *            The SSL engine to use.
	 */
	public SSLEngineManager(SocketChannel channel, SSLEngine engine) {
		this.channel = channel;
		this.engine = engine;

		// Allocate read/write buffers
		SSLSession session = engine.getSession();
		int appBufferSize = session.getApplicationBufferSize();
		int netBufferSize = session.getPacketBufferSize();
		this.appSendBuffer = ByteBuffer.allocate(appBufferSize);
		this.netSendBuffer = ByteBuffer.allocate(netBufferSize);
		this.appRecvBuffer = ByteBuffer.allocate(appBufferSize);
		this.netRecvBuffer = ByteBuffer.allocate(netBufferSize);
	}

	public int read() throws IOException, SSLException {

		// SocketChannels react badly when trying to read at EOF more than once.
		if (engine.isInboundDone()) {
			return -1;
		}

		int pos = appRecvBuffer.position();
		// Read from the channel
		int count = channel.read(netRecvBuffer);

		// Unwrap the data just read
		netRecvBuffer.flip();
		engineResult = engine.unwrap(netRecvBuffer, appRecvBuffer);
		netRecvBuffer.compact();

		// Process the engineResult.status
		switch (engineResult.getStatus()) {
		case BUFFER_UNDERFLOW:
			// netRecvBuffer do not have data
			return 0;
		case BUFFER_OVERFLOW:
			// appRecvBuffer do not have enough space
			throw new BufferOverflowException();
		case CLOSED:
			channel.socket().shutdownInput(); // no more input
			// outbound close_notify will be sent by engine
			break;
		case OK:
			break;
		}

		// process any handshaking
		while (processHandshake())
			;
		if (count == -1) {
			engine.closeInbound();
			// throws SSLException if close_notify not received.
		}
		if (engine.isInboundDone()) {
			return -1;
		}

		// return count of application data read
		count = appRecvBuffer.position() - pos;
		return count;

	}

	public int write() throws IOException, SSLException {
		int pos = appSendBuffer.position();

		netSendBuffer.clear();
		// Wrap the data to be written
		appSendBuffer.flip();
		engineResult = engine.wrap(appSendBuffer, netSendBuffer);
		appSendBuffer.compact();

		// Process the engineResult.Status
		switch (engineResult.getStatus()) {
		case BUFFER_UNDERFLOW:
			throw new BufferUnderflowException();
		case BUFFER_OVERFLOW:
			// Will not occur since there is a flush after every
			// wrap
			throw new BufferOverflowException();
		case CLOSED:
			throw new SSLException("SSLEngine is CLOSED");
		case OK:
			break;
		}

		// Process handshakes
		while (processHandshake())
			;
		// Flush any pending data to the network
		flush();

		// return count of application bytes written.
		return pos - appSendBuffer.position();
	}

	public int flush() throws IOException {
		netSendBuffer.flip();
		int count = channel.write(netSendBuffer);
		netSendBuffer.compact();
		return count;
	}

	/**
	 * Close the session and the channel.
	 * 
	 * @exception IOException
	 *                on any I/O error.
	 * @exception SSLException
	 *                on any SSL error.
	 */
	public void close() throws IOException, SSLException {
		try {
			// Flush any pending output data
			flush();

			if (!engine.isOutboundDone()) {
				engine.closeOutbound();
				// process any handshake
				while (processHandshake())
					;
			} else if (!engine.isInboundDone()) {
				// throws SSLException if close_notify not received.
				engine.closeInbound();
				processHandshake();
			}
		} finally {
			// Close the channel.
			channel.close();
		}
	}

	/**
	 * Process handshake status
	 * 
	 * @return true if handshaking can continue.
	 */
	private boolean processHandshake() throws IOException {
		int count;

		// process the handshake status
		switch (engine.getHandshakeStatus()) {
		case NEED_TASK:
			runDelegatedTasks();
			return false; // can't continue during tasks
		case NEED_UNWRAP:
			// Don't read if inbound is already closed
			count = engine.isInboundDone() ? -1 : channel.read(netRecvBuffer);
			netRecvBuffer.flip();
			engineResult = engine.unwrap(netRecvBuffer, appRecvBuffer);
			netRecvBuffer.compact();
			break;
		case NEED_WRAP:
			appSendBuffer.flip();
			engineResult = engine.wrap(appSendBuffer, netSendBuffer);
			appSendBuffer.compact();
			if (engineResult.getStatus() == SSLEngineResult.Status.CLOSED) {
				// flush the close_notify.
				try {
					count = flush();
				} catch (SocketException exc) {
					// Tried but failed to send close_notify back
					// This can happen if the peer has sent its close_notify and
					// then closed the socket.
					exc.printStackTrace();
				}
			} else {
				// flush without the try/catch, letting any exception propagate.
				count = flush();
			}
			break;
		case FINISHED:
		case NOT_HANDSHAKING:
			// handshaking can cease.
			return false;
		}

		// Check the result of the preceding wrap or unwrap.
		switch (engineResult.getStatus()) {
		case BUFFER_UNDERFLOW: // fall through
		case BUFFER_OVERFLOW:
			// handshaking cannot continue.
			return false;

		case CLOSED:
			if (engine.isOutboundDone()) {
				channel.socket().shutdownOutput(); // stop sending
			}
			return false;
		case OK:
			// handshaking can continue.
			break;
		}
		return true;
	}

	private int threadNumber = 1;

	protected void runDelegatedTasks() {
		Thread delegatedTaskThread = new Thread("SSLEngine.TaskThread-" + (threadNumber++)) {
			public void run() {
				// run delegated tasks
				Runnable task;
				while ((task = engine.getDelegatedTask()) != null) {
					task.run();
				}
			}
		};
		delegatedTaskThread.start();
	}

	public SSLEngine getEngine() {
		return this.engine;
	}

	public ByteBuffer getAppRecvBuffer() {
		return this.appRecvBuffer;
	}

	public ByteBuffer getAppSendBuffer() {
		return this.appSendBuffer;
	}
}
